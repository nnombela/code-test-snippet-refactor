package com.taptapnetworks;

import com.taptapnetworks.codetest.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnit4.class)
public class MailConnectionTest {

    ByteArrayOutputStream myOut;

    @Before
    public void mockSystemOut() {
        myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));
    }

    @Test
    public void send() {
        Properties properties = new Properties();

        // Server connection properties
        properties.setProperty("host", "mail.myserver.com");

        MailConnection connection = new MailConnection(properties);

        String to = "Nicolas";
        String content = "Content";
        String subject = "Subject";
        MailMessage message = new MailMessage(to, subject, content);

        connection.open();
        connection.send(message);
        connection.close();

        assertEquals("Wrong to", to, message.getTo());
        assertEquals("Wrong content", content, message.getContent());
        assertEquals("Wrong content", subject, message.getSubject());

        assertEquals("Wrong output", myOut.toString(), "Created SendMailConnection for server mail.myserver.com\n" +
                "Opening Connection\n" +
                "Preparing message for Nicolas\n" +
                "Subject: Subject\n" +
                "Content\n" +
                "Message sent!!!\n" +
                "Connection closed\n");
    }

}
