package com.taptapnetworks;

import com.taptapnetworks.codetest.HidePasswordEncoder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnit4.class)
public class HidePasswordEncoderTest {

    @Test
    public void encode() {   // you do not need to prefix the method name with "test"
        String password = "Nicolas";
        String hiddenPassword = new HidePasswordEncoder().encode(password);
        assertEquals("Wrong encoding", "*******", hiddenPassword);
    }
}
