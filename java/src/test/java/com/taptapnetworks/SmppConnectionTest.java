package com.taptapnetworks;

import com.taptapnetworks.codetest.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnit4.class)
public class SmppConnectionTest {

    ByteArrayOutputStream myOut;

    @Before
    public void mockSystemOut() {
        myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));
    }

    @Test
    public void send() {
        Properties properties = new Properties();
        // Server connection properties
        properties.setProperty("host", "192.168.1.4");
        properties.setProperty("port", "4301");

        // Credentials connection properties
        properties.setProperty("username", "your-username");
        properties.setProperty("password", "your-password");

        String to = "Nicolas";
        String content = "Content";
        SmppMessage message = new SmppMessage(to, content);
        HidePasswordEncoder passwordEncoder = new HidePasswordEncoder();
        SmppConnection connection = new SmppConnection(passwordEncoder, properties);

        connection.open();
        connection.send(message);
        connection.close();

        assertEquals("Wrong to", to, message.getTo());
        assertEquals("Wrong content", content, message.getContent());

        assertEquals("Wrong output", myOut.toString(), "Logged your-username with password ************* to 192.168.1.4:4301\n" +
                "Sent message to Nicolas\n" +
                "Content\n" +
                "Connection closed\n");
    }

}
