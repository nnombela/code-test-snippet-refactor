package com.taptapnetworks;

import com.taptapnetworks.codetest.MailMessage;
import com.taptapnetworks.codetest.SmppMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(JUnit4.class)
public class MailMessageTest {

    ByteArrayOutputStream myOut;

    @Before
    public void mockSystemOut() {
        myOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myOut));
    }

    @Test
    public void prepare() {
        String to = "Nicolas";
        String content = "Content";
        String subject = "Subject";

        MailMessage message = new MailMessage(to, subject, content);
        message.prepare();

        assertEquals("Wrong output", myOut.toString(), "Preparing message for Nicolas\nSubject: Subject\nContent\n");
    }
}
