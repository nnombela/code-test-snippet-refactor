package com.taptapnetworks.codetest;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Connection {
    protected Properties properties;

    public Connection(Properties properties) {
        this.properties = properties;
    }

    public void open() {
        System.out.println("Opening Connection");
    }

    public void send(Message message) {
        message.prepare(); // preparing is an action that can and should do the message itself
        deliver(message);  // delivering is an action that only a connection can do
    }

    protected abstract void deliver(Message message);

    public void close() {
        System.out.println("Connection closed");
    }

}
