package com.taptapnetworks.codetest;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 10:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class MailConnection extends Connection {
    public MailConnection(Properties properties) {
        super(properties);
        System.out.println(String.format("Created SendMailConnection for server %s", properties.getProperty("host"))); // mailServer is renamed host
    }

    @Override
    protected void deliver(Message message) {
        System.out.println("Message sent!!!");
    }

}
