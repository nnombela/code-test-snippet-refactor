package com.taptapnetworks.codetest;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
interface PasswordEncoder {
    String encode(String password);
}
