package com.taptapnetworks.codetest;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 10:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class HidePasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(String password) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < password.length(); i++) {
            sb.append("*");
        }
        return sb.toString();
    }
}
