package com.taptapnetworks.codetest;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 9:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmppConnection extends Connection {
    private PasswordEncoder passwordEncoder;

    public SmppConnection(PasswordEncoder passwordEncoder, Properties properties) {
        super(properties);
        this.passwordEncoder = passwordEncoder;
    }

    private void login() {
        String hiddenPassword = passwordEncoder.encode(properties.getProperty("password"));
        System.out.println(String.format("Logged %s with password %s to %s:%s", properties.getProperty("username"),
                hiddenPassword, properties.getProperty("host"), properties.getProperty("port")));
    }

    @Override
    public void open() {
        login();
    }

    @Override
    protected void deliver(Message message) {
        System.out.println(String.format("Sent message to %s", message.getTo()));
        System.out.println(message.getContent());
    }

}
