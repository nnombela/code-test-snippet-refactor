package com.taptapnetworks.codetest;

import java.util.Properties;

public class SnippetToRefactor {

    // This method has been refactor and deprecated and should not be longer used
    @Deprecated
    public void send(String type, String message, String to, String subject) {
        if (type == "SMS") {
            Properties properties = new Properties();
            // Server connection properties
            properties.setProperty("host", "192.168.1.4");
            properties.setProperty("port", "4301");

            // Credentials connection properties
            properties.setProperty("username", "your-username");
            properties.setProperty("password", "your-password");

            send(new SmppConnection(new HidePasswordEncoder(), properties),new SmppMessage(to, message));
        } else if (type == "mail") {
            Properties properties = new Properties();

            // Server connection properties
            properties.setProperty("host", "mail.myserver.com");

            send(new MailConnection(properties), new MailMessage(to, subject, message));
        } else {
            throw new IllegalArgumentException("Type " + type + " is unknown");
        }
    }

    public void send(Connection connection, Message message) {
        connection.open();
        connection.send(message);
        connection.close();
    }




}
