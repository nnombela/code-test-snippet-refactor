package com.taptapnetworks.codetest;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 9:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class MailMessage extends Message {

    private String subject;

    public MailMessage(String to, String subject, String content) {
        super(to, content);
        this.setSubject(subject);
    }

    @Override
    public void prepare() {
        System.out.println(String.format("Preparing message for %s", getTo()));
        System.out.println(String.format("Subject: %s", getSubject()));
        System.out.println(getContent());
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
