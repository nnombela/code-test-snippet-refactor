package com.taptapnetworks.codetest;

/**
 * Created with IntelliJ IDEA.
 * User: nnombela
 * Date: 12/4/13
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Message {

    private String to;
    private String content;

    public Message(String to, String content) {
        this.setTo(to);
        this.setContent(content);
    }

    // preparing a message is an action that a message can do itself
    public abstract void prepare();

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
